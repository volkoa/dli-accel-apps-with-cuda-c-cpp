/*
 * Compile: nvcc -arch=sm_70 -o add-error-handling 06-errors/01-add-error-handling.cu -run
 */
#include <stdio.h>
#include <assert.h>

inline cudaError_t checkCuda(cudaError_t result) {
    if (result != cudaSuccess) {
        fprintf(stderr, "CUDA Runtime Error: %s\n", cudaGetErrorString(result));
        assert(result == cudaSuccess);
    }
    return result;
}

void init(int *a, int N) {
    int i;
    for (i = 0; i < N; ++i) {
        a[i] = i;
    }
}

__global__
void doubleElements(int *a, int N) {

    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    int stride = gridDim.x * blockDim.x;

    // for (int i = idx; i < N + stride; i += stride) {
    for (int i = idx; i < N; i += stride) { // FIX
        a[i] *= 2;
    }
}

bool checkElementsAreDoubled(int *a, int N) {
    int i;
    for (i = 0; i < N; ++i) {
        if (a[i] != i * 2)
            return false;
    }
    return true;
}

int main() {
    /*
     * Add error handling to this source code to learn what errors
     * exist, and then correct them. Googling error messages may be
     * of service if actions for resolving them are not clear to you.
     */

    int N = 10000;
    int *a;

    size_t size = N * sizeof(int);
    checkCuda(cudaMallocManaged(&a, size));

    init(a, N);

    /*
     * The previous code (now commented out) attempted to launch
     * the kernel with more than the maximum number of threads per
     * block, which is 1024.
     */

    /*
     * The size of this grid should be 1024*32 = 32768
     */

    size_t threads_per_block = 2048;
    // size_t threads_per_block = 1024; // FIX
    size_t number_of_blocks = 32;

    // Print error:
    //     CUDA Runtime Error: invalid configuration argument

    //void *kernelArgs[] = { (void*) &a, (void*) &N, };
    //checkCuda(
    //    cudaLaunchKernel((const void *) doubleElements, number_of_blocks,
    //        threads_per_block, kernelArgs));

    doubleElements<<<number_of_blocks, threads_per_block>>>(a, N);
    checkCuda(cudaGetLastError());

    checkCuda(cudaDeviceSynchronize());

    bool areDoubled = checkElementsAreDoubled(a, N);
    printf("All elements were doubled? %s\n", areDoubled ? "TRUE" : "FALSE");

    checkCuda(cudaFree(a));

    return 0;
}
