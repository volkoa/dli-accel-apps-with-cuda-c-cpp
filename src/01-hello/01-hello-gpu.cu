/*
 * Compile: nvcc -arch=sm_70 -o hello-gpu 01-hello/01-hello-gpu.cu -run
 */
#include <stdio.h>

void helloCPU() {
#ifdef __CUDA_ARCH__ // macro true on device and false on host.
    printf("Hello from the GPU.\n");
#else
    printf("Hello from the CPU.\n");
#endif
}

/*
 * Refactor the `helloGPU` definition to be a kernel
 * that can be launched on the GPU. Update its message
 * to read "Hello from the GPU!"
 */

/*
 * Error:
 *     01-hello/01-hello-gpu.cu(38): error: a host function call cannot be configured
 *
 *     1 error detected in the compilation of "/tmp/tmpxft_00004abd_00000000-8_01-hello-gpu.cpp1.ii".
 */
// void helloGPU() {
/*
 * The addition of `__global__` signifies that this function
 * should be launced on the GPU.
 */
__global__ void helloGPU() {
#ifdef __CUDA_ARCH__ // macro true on device and false on host.
    printf("Hello from the GPU.\n");
#else
    printf("Hello from the CPU.\n");
#endif
}

int main() {

    helloCPU();

    /*
     * Refactor this call to `helloGPU` so that it launches
     * as a kernel on the GPU.
     */

    dim3 grid(1);
    dim3 block(1);
    // cudaLaunchKernel((const void *) helloGPU, grid, block, (void **) NULL);
    helloGPU<<<grid, block>>>();
    // helloGPU<<<1, 1>>>(); // print out twice

    /*
     * Add code below to synchronize on the completion of the
     * `helloGPU` kernel completion before continuing the CPU
     * thread.
     */

    cudaDeviceSynchronize();
    // When synch is commented out very likely GPU kernel call will not happen.
    // cudaDeviceSynchronize();

    // helloCPU();

    return 0;
}
