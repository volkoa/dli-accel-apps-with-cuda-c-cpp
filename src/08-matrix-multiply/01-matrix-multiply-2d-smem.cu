/*
 * Compile: nvcc -arch=sm_70 -o matrix-multiply-2d 08-matrix-multiply/01-matrix-multiply-2d.cu -run
 *
 * Profile:
nvprof \
  --metrics gld_transactions,gld_throughput,gst_transactions,gst_throughput,\
shared_load_transactions,shared_load_throughput,shared_store_transactions,shared_store_throughput \
  --events shared_ld_bank_conflict,shared_st_bank_conflict,shared_ld_transactions,shared_st_transactions \
  ./matrix-multiply-2d-smem
 */
#include <stdio.h>
#include <assert.h>

// // https://stackoverflow.com/questions/6150368/cuda-and-eclipse-how-can-i-tell-eclipse-that-or-is-part-of-the-syntax
// #if (defined __CDT_PARSER__) || (defined __INTELLISENSE__)
// // this stuff is defined in:
// // #include <crt/host_defines.h>
// #define __global__
// #define __device__
// #define __host__
// #define __shared__
// #endif

inline cudaError_t checkCuda(cudaError_t result) {
    if (result != cudaSuccess) {
        fprintf(stderr, "CUDA Runtime Error: %s\n", cudaGetErrorString(result));
        assert(result == cudaSuccess);
    }
    return result;
}

#define N  64

__global__ void matrixMulGPU(int * a, int * b, int * c) {
    /*
     * Build out this kernel.
     */
    unsigned int row = threadIdx.x + blockDim.x * blockIdx.x;
    unsigned int col = threadIdx.y + blockDim.y * blockIdx.y;

    int val = 0;
    if (row < N && col < N) {
        for (int ii = 0; ii < N; ii++) {
            val += a[row * N + ii] * b[col + ii * N];
        }
    }
    c[row * N + col] = val;
}

/* macro to index a 1D memory array with 2D indices in column-major order */
#define IDXCM( row, col, ld ) ( ( (col) * (ld) ) + (row) )

/* macro to index a 1D memory array with 2D indices in row-major order */
#define IDXRM( row, col, ld ) ( ( (row) * (ld) ) + (col) )

#define THREADS_PER_BLOCK_X 32
#define THREADS_PER_BLOCK_Y 32

//__global__ void smem_matrixMulGPU(int * a, int * b, int * c) {
//    __shared__ int aTile[THREADS_PER_BLOCK_X][THREADS_PER_BLOCK_Y+1],
//        bTile[THREADS_PER_BLOCK_X][THREADS_PER_BLOCK_Y+1];
//
//    int TILE_DIM = blockDim.x; // == THREADS_PER_BLOCK_
//
//    int row = blockIdx.x * blockDim.x + threadIdx.x;
//    int col = blockIdx.y * blockDim.y + threadIdx.y;
//
//    const int tileX = blockDim.x * blockIdx.x;
//    const int tileY = blockDim.y * blockIdx.y;
//
//    int val = 0;
//    if (row < N && col < N) {
//        for (int ii = 0; ii < N / TILE_DIM; ii++) {
//            int rowa = row;
//            int cola = threadIdx.y + TILE_DIM * ii;
//            // (row) * (ld) ) + (col)
//            int idxa = IDXRM(rowa, cola, N);
//            // int idxa = row * N + TILE_DIM * ii + threadIdx.y;
//            aTile[threadIdx.x][threadIdx.y] = a[idxa];
//
//            int rowb = threadIdx.x + ii * TILE_DIM;
//            int colb = col;
//            // (row) * (ld) ) + (col)
//            int idxb = IDXRM(rowb, colb, N);
//            // int idxb = (threadIdx.x + ii * TILE_DIM) * N + col;
//            bTile[threadIdx.x][threadIdx.y] = b[idxb];
//            // int idxb = (threadIdx.x + ii * TILE_DIM) * N + col;
//            // bTile[threadIdx.y][threadIdx.x] = b[idxb];
//
//            // aTile[threadIdx.x][threadIdx.y] = a[row * TILE_DIM + threadIdx.x];
//            // bTile[threadIdx.y][threadIdx.x] = b[threadIdx.y * N + col];
//            __syncthreads();
//            for (int jj = 0; jj < TILE_DIM; jj++) {
//                val += aTile[threadIdx.x][jj] * bTile[jj][threadIdx.y];
//            }
//        }
//        c[row * N + col] = val;
//    }
//}

__global__ void smem_matrixMulGPU(int * a, int * b, int * c) {
    __shared__ int aTile[THREADS_PER_BLOCK_Y][THREADS_PER_BLOCK_X+1],
        bTile[THREADS_PER_BLOCK_Y][THREADS_PER_BLOCK_X+1];

    int TILE_DIM = blockDim.x; // == THREADS_PER_BLOCK_

    int row = blockIdx.y * blockDim.y + threadIdx.y;
    int col = blockIdx.x * blockDim.x + threadIdx.x;

    const int tileX = blockDim.x * blockIdx.x;
    const int tileY = blockDim.y * blockIdx.y;

    int val = 0;
    if (row < N && col < N) {
        for (int ii = 0; ii < N / TILE_DIM; ii++) {
            int rowa = tileY + threadIdx.y;
            int cola = threadIdx.x + ii * TILE_DIM;
            // (row) * (ld) ) + (col)
            int idxa = IDXRM(rowa, cola, N);
            // int idxa = row * N + TILE_DIM * ii + threadIdx.x;
            aTile[threadIdx.y][threadIdx.x] = a[idxa];

            int rowb = threadIdx.y + ii * TILE_DIM;
            int colb = tileX + threadIdx.x;
            // (row) * (ld) ) + (col)
            int idxb = IDXRM(rowb, colb, N);
            // int idxb = (threadIdx.y + ii * TILE_DIM) * N + col;
            bTile[threadIdx.y][threadIdx.x] = b[idxb];

            __syncthreads();
            for (int jj = 0; jj < TILE_DIM; jj++) {
                val += aTile[threadIdx.y][jj] * bTile[jj][threadIdx.x];
            }
        }
        c[row * N + col] = val;
    }
}


//__global__ void MatMul_smem(int* A, int* B, int* C)
//{
//    float CValue = 0;
//
//    int TILE_DIM = blockDim.x; // == THREADS_PER_BLOCK_
//    int Row = blockIdx.y*TILE_DIM + threadIdx.y;
//    int Col = blockIdx.x*TILE_DIM + threadIdx.x;
//
//    __shared__ int As[THREADS_PER_BLOCK_Y][THREADS_PER_BLOCK_X];
//    __shared__ int Bs[THREADS_PER_BLOCK_Y][THREADS_PER_BLOCK_X];
//
//    int ACols, ARows, BCols, BRows, CRows, CCols;
//    ACols = ARows = BCols = BRows = CRows = CCols =N;
//
//    for (int k = 0; k < (TILE_DIM + ACols - 1)/TILE_DIM; k++) {
//
//         if (k*TILE_DIM + threadIdx.x < ACols && Row < ARows)
//             As[threadIdx.y][threadIdx.x] = A[Row*ACols + k*TILE_DIM + threadIdx.x];
//         else
//             As[threadIdx.y][threadIdx.x] = 0.0;
//
//         if (k*TILE_DIM + threadIdx.y < BRows && Col < BCols)
//             Bs[threadIdx.y][threadIdx.x] = B[(k*TILE_DIM + threadIdx.y)*BCols + Col];
//         else
//             Bs[threadIdx.y][threadIdx.x] = 0.0;
//
//         __syncthreads();
//
//         for (int n = 0; n < TILE_DIM; ++n)
//             CValue += As[threadIdx.y][n] * Bs[n][threadIdx.x];
//
//         __syncthreads();
//    }
//
//    if (Row < CRows && Col < CCols)
//        C[((blockIdx.y * blockDim.y + threadIdx.y)*CCols) +
//           (blockIdx.x * blockDim.x)+ threadIdx.x] = CValue;
//}

/*
 * This CPU function already works, and will run to create a solution matrix
 * against which to verify your work building out the matrixMulGPU kernel.
 */

void matrixMulCPU(int * a, int * b, int * c) {
    int val = 0;

    for (int row = 0; row < N; ++row)
        for (int col = 0; col < N; ++col) {
            val = 0;
            for (int k = 0; k < N; ++k)
                val += a[row * N + k] * b[k * N + col];
            c[row * N + col] = val;
        }
}

int main() {
    int *a, *b, *c_cpu, *c_gpu; // Allocate a solution matrix for both the CPU and the GPU operations

    int size = N * N * sizeof(int); // Number of bytes of an N x N matrix

    // Allocate memory
    cudaMallocManaged(&a, size);
    cudaMallocManaged(&b, size);
    cudaMallocManaged(&c_cpu, size);
    cudaMallocManaged(&c_gpu, size);

    // Initialize memory; create 2D matrices
    for (int row = 0; row < N; ++row)
        for (int col = 0; col < N; ++col) {
            a[row * N + col] = row;
            b[row * N + col] = col + 2;
            c_cpu[row * N + col] = 0;
            c_gpu[row * N + col] = 0;
        }

    /*
     * Assign `threads_per_block` and `number_of_blocks` 2D values
     * that can be used in matrixMulGPU above.
     */

    dim3 threads_per_block(THREADS_PER_BLOCK_X, THREADS_PER_BLOCK_Y);
    size_t nblocks_x = (N + threads_per_block.x - 1) / threads_per_block.x;
    size_t nblocks_y = (N + threads_per_block.y - 1) / threads_per_block.y;
    dim3 number_of_blocks(nblocks_x, nblocks_y);

    // matrixMulGPU <<< number_of_blocks, threads_per_block >>>(a, b, c_gpu);
    smem_matrixMulGPU <<< number_of_blocks, threads_per_block >>>(a, b, c_gpu);
    // MatMul_smem <<< number_of_blocks, threads_per_block >>>(a, b, c_gpu);

    cudaDeviceSynchronize();

    // Call the CPU version to check our work
    matrixMulCPU(a, b, c_cpu);

    // Compare the two answers to make sure they are equal
    bool error = false;
    for (int row = 0; row < N && !error; ++row) {
        for (int col = 0; col < N && !error; ++col) {
            if (c_cpu[row * N + col] != c_gpu[row * N + col]) {
                printf("FOUND ERROR at c[%d][%d]\n", row, col);
                error = true;
                break;
            }
        }
    }

    if (!error) {
        printf("Success!\n");
    }

    // Free all our allocated memory
    cudaFree(a);
    cudaFree(b);
    cudaFree(c_cpu);
    cudaFree(c_gpu);

    return 0;
}
