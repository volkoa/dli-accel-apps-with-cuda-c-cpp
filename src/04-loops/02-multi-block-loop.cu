/*
 * Compile: nvcc -arch=sm_70 -o multi-block-loop 04-loops/02-multi-block-loop.cu -run
 */
#include <stdio.h>

/*
 * Refactor `loop` to be a CUDA Kernel. The new kernel should
 * only do the work of 1 iteration of the original loop.
 */

__global__ void loop(int N) {
    unsigned int ii = threadIdx.x + blockIdx.x * blockDim.x;
    if (ii < N) {
        printf("This is iteration number %d\n", ii);
    }
}

int main() {
    /*
     * When refactoring `loop` to launch as a kernel, be sure
     * to use the execution configuration to control how many
     * "iterations" to perform.
     *
     * For this exercise, be sure to use more than 1 block in
     * the execution configuration.
     */

    int N = 10;
    int nblocks = 2;
    // fast-ceiling-of-an-integer-division-in-c-c
    int nthreads_per_block = N/nblocks + (N % nblocks != 0);
    loop<<<nblocks, nthreads_per_block>>>(N);

    cudaDeviceSynchronize();

    return 0;
}
